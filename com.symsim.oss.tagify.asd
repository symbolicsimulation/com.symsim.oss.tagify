;;; -*- Mode: LISP; Syntax: Ansi-Common-Lisp; Base: 10; Package: cl-user; -*- 

;;;
;;; Copyright Symbolic Simulation, LLC, 2012.
;;; 
;;; License "Golden Rule License"
;;;
;;; First   (load "com.symsim.symbology.asd")
;;; then    (asdf:oos 'asdf:load-op :com.symsim.symbology)
;;; finally (com.symsim.symbology:run)
;;;

(require :swank)

(defpackage #:com.symsim.oss.tagify.asd
  (:use #:cl #:asdf))

;;;;
;;;; This is a forward declaration of the main package so that the
;;;; (com.symsim.symbology::load-jms) call below can be here in the system
;;;; definition :perform 'load-op :after, making this system easier to
;;;; include in others which require symbology
;;;;

(defpackage #:com.symsim.oss.tagify
  (:use #:cl #:asdf)
  (:export #:tagify))

(in-package :com.symsim.oss.tagify.asd)

(defsystem #:com.symsim.oss.tagify
  :name "com.symsim.oss.tagify"
  :version "0.0.1"
  :maintainer "jm@symbolic-simulation.com"
  :author "jm@symbolic-simulation.com"
  :licence "Golden Rule License"
  :description "com.symsim.oss.tagify implementation"
;;  :depends-on (#:swank #:metatilities)
  :depends-on (#:swank)
  :components ((:file "tagify"))
  :perform (load-op :after (op s)
		    (format t "~&;; try (com.symsim.oss.tagify:tagify <system-name>)~%")))
