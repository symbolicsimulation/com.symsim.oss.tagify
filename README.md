# com.symsim.oss.tagify #


### What is this repository for? ###

* This quicklisp-loadable library can create an emacs tag file by walking the ASDF hierarchy, and running etags on each file.  Currently, it is obsessive, and walks the hierarchy to the bitter end.


Runs etags on all files of the named, loaded, ASDF system.

* Version 0.0.1

### How do I get set up? ###

This is a prototype GUI for quicklisp.

* use mercurial to clone this project into ~/quicklisp/local-projects
* load your quicklisp or ASDF system of choice
* `(ql:quickload :com.symsim.oss.tagify)`
* `(com.symsim.oss.ql-gui:tagify <system-symbol>)`


### Contribution guidelines ###

* n/a

### Who do I talk to? ###

* jm@symbolic-simulation.com
