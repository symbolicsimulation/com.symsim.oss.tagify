;;; -*- Mode: LISP; Syntax: Ansi-Common-Lisp; Base: 10; Package: com.symsim.oss.tagify; -*- 

;;;
;;; Copyright Symbolic Simulation, LLC, 2017.
;;; 
;;; License "Golden Rule License"
;;;
;;; First   (load "com.symsim.oss.tagify.asd")
;;; then    (asdf:oos 'asdf:load-op :com.symsim.oss.tagify)
;;; finally (com.symsim.oss.tagify::tagify <system-name>)
;;;

(in-package :com.symsim.oss.tagify)

(defvar *list-of-systems* nil)
(defvar *list-of-files* nil)

(defun tagify-file (file)
  #+NIL (format t "        FILE ~S/~S~%" file (slot-value file 'asdf::name))
  #+NIL (format t "        path ~S~%" (slot-value file 'asdf::absolute-pathname))
  (pushnew
   (namestring (slot-value file 'asdf::absolute-pathname))
   *list-of-files*
   :test 'string-equal))

(defun tagify-module (module)
  #+NIL (clouseau:inspector module)
  #+NIL (format t "    MODULE ~S~%" (slot-value module 'asdf::name))
  (dolist (child (slot-value module 'asdf::children))
    (tagify-file child)))

(defun internal-tagify-system (system)
  #+NIL (clouseau:inspector system)
  (unless (member system *list-of-systems*)
    #-NIL (format t "internal-tagify-system ~S~%" system)
    #+NIL (format t "internal-tagify-system ~S~%" (slot-value system 'asdf::name))
    #+NIL (format t "   ~A: source-file ~S~%" system (slot-value system 'asdf:source-file))
    (pushnew system *list-of-systems*)
    (dolist (sideway-dependency (asdf:component-sideway-dependencies system))
      (tagify-system sideway-dependency))
    (dolist (child (slot-value system 'asdf::children))
      (cond ((equal 'asdf/component::module (type-of child))
	     (tagify-module child))
	    ((subtypep (type-of child) 'asdf/lisp-action::cl-source-file)
	     (tagify-file child))
	    ((subtypep (type-of child) 'asdf/component::source-file)
	     (tagify-file child))
	    ((equal 'swank-loader::swank-loader-file (type-of child))
	     nil)
	    (t
	     (break "unknown child"))))))

(defun tagify-system (sys)
  #-NIL (format t "tagify-system ~S~%" sys)
  (cond ((null sys)
	 nil)
	((or (stringp sys) (keywordp sys) (symbolp sys))
	 (handler-case
	     (let* ((asdf-sys (asdf:find-system sys))
		    (type-of-asdf-sys (type-of asdf-sys)))
	       (internal-tagify-system asdf-sys))
	   (error (err)
	     (break (format nil "asdf:find-system err ~s, sys ~s, type-of ~s" err sys (type-of sys))))))
	((and (listp sys)
	      (eq :VERSION (first sys)))
	 (tagify-system (second sys)))
	((and (listp sys)
	      (or
	       (eq :FEATURE (first sys))
	       (eq :REQUIRE (first sys))))
	 nil)
	(t
	 (error (err)
	   (break (format nil "err ~s, sys ~s, type-of ~s" err sys (type-of sys)))))))

(defun tagify (sys)
  (setf *list-of-systems* nil)
  (setf *list-of-files* nil)
  (tagify-system sys)
  (pprint *list-of-systems*)
  (uiop/run-program:run-program
   (append
    (list "etags" "--language=lisp")
    *list-of-files*)))


#+NIL (tagify :com.symsim.oss.postgis-gui)
